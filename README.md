# World Space Crosshair

A crosshair for Unity that exists in the 3D world space instead of screen space. Useful for interaction with 3D objects.

!["Example of Adaptive Scaling""](https://i.imgur.com/EJNfZ4W.gif "Example of Adaptive Scaling")
<br>Example of Adaptive Scaling

As with all 2D, screen space UI elements, crosshairs as typically implemented are not suitable for XR content due to the stereoscopic separation of each eye. However, traditional UI elements like crosshairs are still useful for developing HUDs and other UI for XR content. This project was created to solve that dilemma.

### Quick Set-up 
Go ahead and clone or download this repository -- the "World Space Crosshair" directory can be placed into your Assets directory.

There are two included prefabs that can be used to immediately get a World Space Crosshair up and running in your project: the Crosshair Projector and the Independent Crosshair. After choosing the right prefab for your project, just drag it into your hierarchy and customize the options (see below).<br>
<br>1) The Crosshair Projector consists of two nested objects: the parent projector and the child crosshair. This prefab should be used when you don't have your own way of moving the crosshair around. The projector base will rotate around with the mouse by default, which will move the crosshair itself around as its child. For more info on how this works, please refer to CrosshairProjector.cs and the next section (Summary).
<br><br>2) The Independent Crosshair is a single prefab that will allow you to place a 3D crosshair in your project without using the pre-made projector. Most people will want to use this option, but it requires a little more forethought and understanding of how the plugin works. The Independent Crosshair will need to be attached to a different rotating parent object, which means you must create this input yourself. A quick example helps illustrate one of the uses of this prefab: if you have a camera that rotates along with the mouse (as the Unity-provided character controller does), attaching the Independent Crosshair as a child of this camera will make it follow the camera's rotation while still adjusting to the correct 3D depth. If you use an Independent Crosshair, make sure to set your Raycast Origin object carefully. This is usually the parent object (weapon/camera/etc) of the Independent Crosshair object.

### Internal Details

**Placing the Crosshair**
<br>A game object (called the "Raycast Origin") is used as the origin of a ray that determines the correct depth for the crosshair. The crosshair will set its location to the impact point of the ray from the Raycast Origin, which means that rotating or moving this Raycast Origin (aka projector) will move the crosshair. Therefore, your projector should likely be a child of the camera or player object.

**Shader Details**
<br>I've included a special shader to make sure that the crosshair isn't obscured by world geometry: "TranspUnlitShaderZtestOff.shader". If you want to use another shader, just make sure to turn off Z testing.

### Customization Options 

The following options are available on the WorldCrosshair.cs component:
<br><br>**Raycast Origin:**
<br> The origin of the ray which to determines the correct depth for the crosshair. Usually this is the parent object of the crosshair itself. 
<br>**Max Sense Distance:** 
<br>The maximum distance that the raycast will extend for, which means anything beyond this distance will not be registered and the crosshair will stay at this distance. If this is set to <= 0, the main camera's (tag: "Main Camera") far clip plane distance will be used instead. 
<br>**Update Interval:** 
<br>The amount of time (in seconds) between raycasts. If set to <= 0, it will default to raycasting every frame. Normally this can be left at 0, but if you experience performance issues (likely due to a large number of physics interactions or colliders in the scene), you can increase this. The higher this number is, the more time there will be between raycasts, which means anything above very small numbers will have a noticeable delay on the crosshair's movement.
<br>**Use Fixed Update:** 
<br>If this is checked, the crosshair will update with the physics engine during FixedUpdate instead of during the regular Update. Most will want to leave this unchecked unless you're using the crosshair to interact with the physics system in some capacity. 
<br>**Enable Adaptive Scaling:** 
<br>Adaptive Scaling means that the crosshair will try to maintain the same relative size on screen regardless of the distance that it is from the camera, which is especially helpful for XR content. You should experiment with this option on and off to see which one works best for you and your project. Note that this variable cannot be changed at runtime without manually resetting the crosshair scale, as the state of this variable isn't tracked across frames. If you want to change this at runtime, you'll need to modify the code to keep track of state changes to this variable or just toggle between two different crosshair objects that have different settings.
<br>**Stereoscopic Scale Tweak:** 
<br>When the crosshair is very close to the player or camera, it can sometimes give the sensation that it is very small due to the effects of stereoscopic vision. This might be a weird feeling and it's mitigated by increasing the size of the crosshair when it's close to the Raycast Origin. Turn this option on to enable this size change, and turn it off to disable it. The benefits of this can be dependent on the unique player and might only be useful for some projects or players. Therefore, you should experiment with this in your project and you may also wish to expose this option in your GUI so that players can choose for themselves. 
<br>**Depth Offset:** 
<br>The crosshair will be offset by this distance from the raycast's point of collision with your world's geometry. For most cases, this should be left at 0, but if you need to tweak this option, it is exposed and can be changed at runtime. Positive numbers will mean that the crosshair is pushed farther away from the Raycast Origin and negative numbers will move it towards the Raycast Origin.
<br><br>**Changing the Crosshair Texture:**
<br>If you want a different texture for your crosshair, edit the m_Crosshair material in the Images folder of the plugin. This will change all of the crosshairs in your project to the new texture. If you wish to use multiple crosshair textures, you can create multiple materials (one for each texture you would like to use). You can even create fancy effects like an animated crosshair with some creativity.




### Contribution and Ideas for Improvement 
This project was initially released on the Unity Asset Store in 2015. It was maintained for quite some time but has never been my primary focus. Therefore, I'm releasing it as open-source in case others find it useful.
If you use this in your project, please let me know and consider crediting me! I'd love to see what you're doing with it.

Merge requests are welcome! The following ideas aren't implemented, but would be great improvements.
<br>1) Optional boundaries for the crosshair projector, preventing the crosshair from traveling offscreen or outside a certain range.
<br>2) UI activation capabilities, allowing the crosshair to interact with the standard Unity UI selectables such as buttons or check boxes. Most of this is already implemented in the [World Space Cursor](https://gitlab.com/metaphoricalmedia/world-space-cursor) project. Ideally, these projects can be combined to create a unified XR & world space interface solution for Unity.
<br>3) Custom inspector for the crosshair component, which would simplify the integration process further and suggest certain ranges for the values of the customization options.
<br>4) Ignore layers, which would allow the crosshair to ignore certain layers. Useful for windows, transparent objects, complex UI, etc.
